import * as vscode from 'vscode';
import {
	LanguageClient,
	LanguageClientOptions,
	RevealOutputChannelOn,
	ServerOptions,
	TransportKind
  } from 'vscode-languageclient';

let client: LanguageClient;

export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "hsinspect" is now active!');

	const runArgs: string[] = [];
    const debugArgs: string[] = [];
    // Replace this with path to hsinspect-lsp
	const serverPath = "/Users/rahulmuttineni/Development/rahulmutt/hsinspect/dist-newstyle/build/x86_64-osx/ghc-8.6.5/hsinspect-lsp-0.0.1/x/hsinspect-lsp/build/hsinspect-lsp/hsinspect-lsp";

	const serverOptions: ServerOptions = {
		run: { command: serverPath, transport: TransportKind.stdio, args: runArgs },
		debug: { command: serverPath, transport: TransportKind.stdio, args: debugArgs }
	  };

	const clientOptions: LanguageClientOptions = {
		documentSelector: [
			{ scheme: 'file', language: 'haskell' },
		],
	};

	client = new LanguageClient(
		'hsinspect-lsp',
		'hsinspect Language Server',
		serverOptions,
		clientOptions
	);

	client.start();
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
